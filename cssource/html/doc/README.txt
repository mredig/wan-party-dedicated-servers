[COLOR=#ff6600][SIZE=5][b]DeathMatch[/b][/SIZE][/COLOR]

[b]Table of contents[/b]
--------------------------------------------------------------
[list]
    [*] [goanchor=Description]Description[/goanchor]
    [*] [goanchor=Complementary plugins]Complementary plugins[/goanchor]
    [*] [goanchor=Plugins]Plugins[/goanchor]
    [*] [goanchor=Commands and Cvars]Commands and Cvars[/goanchor]
    [*] [goanchor=Requirements]Requirements[/goanchor]
    [*] [goanchor=Installation]Installation[/goanchor]
    [*] [goanchor=Credits]Credits[/goanchor]
    [*] [goanchor=Changelog]Changelog[/goanchor]
    [*] [goanchor=Todo]Todo[/goanchor]
    [*] [goanchor=Issues]Issues[/goanchor]
[/list]
[anchor]Description[/anchor][b]Description[/b]
--------------------------------------------------------------
    [indent]Adds deathmatch mode, elimination mode, spawn protection to the game.
    Spawn protection works independently from deathmatch inself, you can
    use in separately. Usefull for gungame.
    
    http://forums.alliedmods.net/showthread.php?t=103242
    [/indent]

[anchor]Complementary plugins[/anchor][b]Complementary plugins[/b]
--------------------------------------------------------------
[list]
    [*] disable round end
        [b]sm_bot_endround[/b] - http://forums.alliedmods.net/showthread.php?t=80662
    [*] team balancer
        [b]sm_teambalance[/b] - http://forums.alliedmods.net/showthread.php?t=117956
[/list]
[anchor]Plugins[/anchor][b]Plugins[/b]
--------------------------------------------------------------
[list]
    [*] [b]sm_ggdm.smx[/b]                  - Deathmatch
    [*] [b]sm_ggdm_elimination.smx[/b]      - Elimination
    [*] [b]sm_ggdm_spawnprotection.smx[/b]  - Spawn protection
    [*] [b]sm_ggdm_spawns.smx[/b]           - Random spawnpoints
    [*] [b]sm_ggdm_ragdoll.smx[/b]          - Ragdoll removement
    [*] [b]sm_ggdm_weapons.smx[/b]          - Weapons removement
    [*] [b]sm_ggdm_firstspawn.smx[/b]       - Spawn new connected players
[/list]
[anchor]Commands and Cvars[/anchor][b]Commands and Cvars[/b]
--------------------------------------------------------------
[list]
    [*] [b]sm_ggdm_version[/b]              - GunGame Deathmatch version.
    [*] [b]sm_ggdm_respawntime[/b]          - Time before respawn (default 2.0).
    [*] [b]sm_ggdm_enable[/b]               - Enable/disable deathmatch (default 1).

    [*] [b]sm_ggdm_removeragdolls[/b]       - Remove ragdolls 1 is on, 0 is off (default 1).
    [*] [b]sm_ggdm_ragdolltime[/b]          - Time before ragdoll will be removed (default 1.5).

    [*] [b]sm_ggdm_sp_enable[/b]            - Enable/disable spawn protection (default 1).
    [*] [b]sm_ggdm_sp_removeonfire[/b]      - Remove spawn protection when player fire (default 0).
    [*] [b]sm_ggdm_sptime[/b]               - Sets the amount of seconds users will be protected 
                                              from getting killed on their respawn (default 2).
    [*] [b]sm_ggdm_spchangecolor[/b]        - Change color on spawn protected players (default 1).

    [*] [b]sm_ggdm_remweap_round[/b]        - Remove weapons on round start (default "1").

    [*] [b]sm_ggdm_remweap_kill[/b]         - Remove weapons on player kill (default "0").

    [*] [b]sm_ggdm_keep_hegrenades[/b]      - Keep hegrenades when removing weapons (default "1").

    [*] [b]sm_ggdm_elimination[/b]          - Elimination mode 1 is on, 0 is off (default 0). You 
                                              will be respawned only after your killer will be killed.
                                              To enable elimination sm_ggdm_enable must be 0
                                              and sm_ggdm_elimination must be 1.
    [*] [b]sm_ggdm_el_respawntime[/b]       - Elimination respawn time (default 2.0).
    [*] [b]sm_ggdm_elimination_spawn[/b]    - Spawn new connected players in the middle of the round 
                                              1 - enabled, 0 - disabled (default: 1)

    [*] [b]sm_ggdm_spawn_menu[/b]           - Edits GG DM spawn points.

    [*] [b]sm_ggdm_fspawn_enable[/b]        - Enable firstspawn plugin.
                                              1 - enable, 0 - disable (default: 0).
    [*] [b]sm_ggdm_fspawn_ptime[/b]         - Prevent player from spawn if it was reconnected 
                                              in that number of seconds. (default: 420).

[/list]
[anchor]Requirements[/anchor][b]Requirements[/b]
--------------------------------------------------------------
[list]
    [*] Counter-Strike: Source
    [*] SourceMod 1.2.0+
[/list]
[anchor]Credits[/anchor][b]Credits[/b]
--------------------------------------------------------------
[list]
    [*] Thanks to tigerox for GG4 DEATHMATCH ADDON version 1.2
        http://forums.alliedmods.net/showthread.php?t=87198
    [*] Thanks to TechKnow for Gungame-Deathmatch version 1.5
        http://forums.alliedmods.net/showthread.php?p=725749
    [*] Thanks to L. Duke for  Dissolve (player ragdolls) version 1.0.0.2
        http://forums.alliedmods.net/showthread.php?t=71084
    [*] Thanks to Fredd for SurfTools version 1.6
        http://forums.alliedmods.net/showthread.php?t=60002
    [*] Thanks to AlliedModders LLC for CSS:DM 2.1.0.446
        http://forums.alliedmods.net/showthread.php?t=58388
        GGDM Spawns is based on CSS:DM Spawns.
    [*] Thanks to Kigen for weapon removement on player death code.
[/list]
[anchor]Changelog[/anchor][b]Changelog[/b]
----------------------------------------------------
    [indent]For full changelog see CHANGELOG.txt[/indent]

[anchor]Todo[/anchor][b]Todo[/b]
----------------------------------------------------
    [indent]For full todo list see TODO.txt[/indent]

[anchor]Issues[/anchor][b]Issues[/b]
----------------------------------------------------
[list]
    [*] [b]Q.[/b] After mp_teambalance move player to the other 
                    team the player does not respawn.
        [b]A.[/b] Yes, it does. This is because of mp_limitteams does not
                    designed to work with deathmatch mode at all. You can
                    use any 3d-party team balance plugins like sm_teambalance 
                    http://forums.alliedmods.net/showthread.php?t=117956
                    
                    Player can respawn only after "jointeam". But mp_limitteams
                    does not execute jointeam but only assign player to team.
[/list]

