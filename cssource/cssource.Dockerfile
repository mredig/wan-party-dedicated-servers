FROM docker.io/ubuntu:22.04

# Insert Steam prompt answers
SHELL ["/bin/bash", "-o", "pipefail", "-c"]
RUN echo steam steam/question select "I AGREE" | debconf-set-selections \
 && echo steam steam/license note '' | debconf-set-selections

# Update the repository and install SteamCMD
ARG DEBIAN_FRONTEND=noninteractive

RUN dpkg --add-architecture i386
RUN apt update
RUN apt install -y \
cpio \
xz-utils \
curl \
wget \
file \
tar \
bzip2 \
gzip \
unzip \
bsdmainutils \
python3 \
util-linux \
ca-certificates \
binutils \
bc \
jq \
tmux \
netcat \
lib32gcc-s1 \
lib32stdc++6 \
locales \
distro-info \
hostname \
util-linux \
sudo \
iproute2 \
uuid-runtime \
libtinfo5:i386


# Install SteamCMD
RUN echo "**** Install SteamCMD ****" \
    && echo steam steam/question select "I AGREE" | debconf-set-selections \
    && echo steam steam/license note '' | debconf-set-selections \
    && dpkg --add-architecture i386 \
    && apt-get update -y \
    && apt-get install -y --no-install-recommends libsdl2-2.0-0:i386 steamcmd \
    && ln -s /usr/games/steamcmd /usr/bin/steamcmd

# # Install NodeJS
# RUN echo "**** Install NodeJS ****" \
#     && curl -sL https://deb.nodesource.com/setup_16.x | bash - \
#     && apt-get update && apt-get install -y nodejs

# # Install GameDig https://docs.linuxgsm.com/requirements/gamedig
# RUN echo "**** Install GameDig ****" \
#     && npm install -g gamedig

# Install Cleanup
RUN echo "**** Cleanup ****"  \
    && apt-get -y autoremove \
    && apt-get -y clean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* \
    && rm -rf /var/tmp/*

RUN rm -rf /var/lib/apt/lists/*


# Add unicode support
RUN locale-gen en_US.UTF-8
ENV LANG 'en_US.UTF-8'
ENV LANGUAGE 'en_US:en'

ARG USERNAME=linuxgsm
ARG USER_UID=1000
ARG USER_GID=$USER_UID

## Add linuxgsm user
RUN echo "**** Add linuxgsm user ****" \
# Create the user
    && groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    #
    # [Optional] Add sudo support. Omit if you don't need to install software after connecting.
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME \
    && chown $USERNAME:$USERNAME /home/$USERNAME

RUN mkdir linuxgsm
RUN chmod 777 linuxgsm

USER linuxgsm

WORKDIR linuxgsm
RUN wget -O linuxgsm.sh https://linuxgsm.sh
RUN chmod +x linuxgsm.sh
RUN bash linuxgsm.sh cssserver
RUN ./cssserver auto-install

COPY cfg_files/* /linuxgsm/serverfiles/cstrike/cfg/
COPY html/maps/* /linuxgsm/serverfiles/cstrike/maps/
COPY html/materials /linuxgsm/serverfiles/cstrike/materials/
COPY html/sound /linuxgsm/serverfiles/cstrike/sound/
COPY html/cfg /linuxgsm/serverfiles/cstrike/cfg/
COPY html/doc /linuxgsm/serverfiles/cstrike/doc/
COPY html/models /linuxgsm/serverfiles/cstrike/models/
COPY addons /linuxgsm/serverfiles/cstrike/addons/


WORKDIR /linuxgsm/serverfiles
ENTRYPOINT ["./srcds_run", "-game cstrike", "-strictportbind", "-ip 0.0.0.0", "-port 27015", "+clientport 27005", "+tv_port 27020", "+sv_setsteamaccount", "+map de_dust2", "+servercfgfile cssserver.cfg", "-maxplayers 16"]
