FROM docker.io/ubuntu:16.04

ARG DEBIAN_FRONTEND=noninteractive

RUN dpkg --add-architecture i386
RUN apt update
RUN apt install -y \
curl \
unzip 

RUN echo "**** Cleanup ****"  \
    && apt-get -y autoremove \
    && apt-get -y clean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/* \
    && rm -rf /var/tmp/*

RUN rm -rf /var/lib/apt/lists/*

RUN mkdir serverfiles
COPY release-linux-x86_64.zip /
RUN unzip release-linux-x86_64 -d serverfiles/

WORKDIR /serverfiles

RUN mv release-linux-x86_64/* .
RUN rm -rf release-linux-x86_64

RUN curl -O https://s3.us-west-1.wasabisys.com/mredig-fileshare/dedicated-server-assets/baseq3.zip
RUN curl -O https://s3.us-west-1.wasabisys.com/mredig-fileshare/dedicated-server-assets/matrix.zip
RUN curl -O https://s3.us-west-1.wasabisys.com/mredig-fileshare/dedicated-server-assets/missionpack.zip
RUN curl -O https://s3.us-west-1.wasabisys.com/mredig-fileshare/dedicated-server-assets/q3ut2-4.zip
RUN curl -O https://s3.us-west-1.wasabisys.com/mredig-fileshare/dedicated-server-assets/q3ut3.zip

RUN unzip -nu baseq3.zip
RUN unzip -nu matrix.zip
RUN unzip -nu missionpack.zip
RUN unzip -nu q3ut2-4.zip
RUN unzip -nu q3ut3.zip

RUN rm -rf __MACOSX
RUN rm *.zip

COPY q3_files/* baseq3/
COPY ut24_files/* q3ut2-4/
COPY ut32_files/* q3ut3/
COPY matrix_files/* matrix/
COPY q3_html/* baseq3/
COPY ut24_html/* q3ut2-4/
COPY ut32_html/* q3ut3/
COPY matrix_html/* matrix/

COPY entrypoint.sh .

ENTRYPOINT ["./entrypoint.sh", "missionpack", "q3dm1", "27961"]