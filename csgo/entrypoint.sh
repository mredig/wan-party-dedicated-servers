#!/usr/bin/env sh

echo "Logging in with $STEAMID"
./srcds_run \
-game csgo \
-usercon \
-strictportbind \
-ip 0.0.0.0 \
-port 27017 \
+clientport 27007 \
+tv_port 27030 \
+sv_setsteamaccount \
$STEAMID \
-tickrate 64\
 +map de_mirage \
 +servercfgfile csgoserver.cfg \
 -maxplayers_override 16 \
 +mapgroup mg_active \
 +game_type 0 \
 +game_mode 0 \
 +host_workshop_collection \
 +workshop_start_map \
 -authkey \
 -nobreakpad
