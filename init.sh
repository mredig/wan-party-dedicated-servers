#!/usr/bin/env bash
set -o xtrace

PREBUILD_SERVICES=($(echo $PREBUILD_SERVICES | tr ";" "\n"))

# downloads and runs a script in my `vm-init-scripts` repo hosted on gitlab. Additional parameters are passed to the script as arguments
loadAndRunInitScript () {
	PARAMS=($@) # copy $@ to PARAMS
	unset PARAMS[0] # remove the script name from args passed to the script
	SCRIPT=$1 # store the script name

	curl -o $SCRIPT.sh "https://gitlab.com/mredig/vm-init-scripts/-/raw/main/$SCRIPT.sh"
	chmod +x $SCRIPT.sh
	./$SCRIPT.sh ${PARAMS[*]} # run the script, passing all its args
	rm $SCRIPT.sh
}

echo "+++=========DYN DUCK DNS===========+++"
curl "https://www.duckdns.org/update?domains=$DUCKDNSHOST&token=$DUCKDNSTOKEN"
echo "updated dyn duck ip"

echo "+++=========HOME ROOT SETUP===========+++"
su root
cd /root
export HOME=/root

touch "firstBoot"

echo "+++=========SSH SETUP===========+++"
loadAndRunInitScript "install_ssh_keys"
echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCM1tb7fu84KezfvJnalnl3V0r+0TyP4F46ftMXtWa6/HMxji94DT/A4jZmK5XPl595VKUYH/9QV87NA2xi36dVu6oWrXRiLlo5tSjIqJQbb/JysxcpwS0eb4BQROVmxB19pKBOLOwHByXLobCW530W6S+XtakAy+KsmSajanXInyJSYDs/ZHyZ2N2WeJbRkyRu76PPii5Y951scJr6EKJw3vCsbebL/A3dwd3P0bkuspnUR5klJ7UkNc9qonOTYGCeCi0eFDO4hEdUg8UtpjJ7bSc4Ygfejkq7DYq70/tIsht01ITG3dfV8jTcb5l5P0/EUxp++IHB+NWPTxwtEQf9 rsa-key-20221217
" >> /root/.ssh/authorized_keys

echo "+++=========UPDATES AND GIT SETUP===========+++"
loadAndRunInitScript "apt_update_and_install" "git" "git-lfs"
git config --global user.email "he@ho.hum"
git config --global user.name "Wan Party Cloud Server"
git lfs install

echo "+++=========DOCKER SETUP===========+++"
loadAndRunInitScript "install_docker"
source ~/.bashrc

echo "+++=========DEDICATED SERVER COMPOSE SETUP===========+++"
echo "export STEAMID=$STEAMID" >> ~/.bashrc
source ~/.bashrc
git clone https://gitlab.com/mredig/wan-party-dedicated-servers.git
cd wan-party-dedicated-servers

docker compose build ${PREBUILD_SERVICES[*]}

docker compose up -d ${PREBUILD_SERVICES[*]}

echo "+++=========FIX SSH===========+++"
cd /etc/ssh

sed 's/#ListenAddress 0/ListenAddress 0/g' sshd_config > sshd_config.2
mv sshd_config.2 sshd_config

echo "+++=========BUILD ALL IN BG===========+++"
echo "@reboot root cd ~/wan-party-dedicated-servers/; export STEAMID=$STEAMID; docker compose --profile allServers build; rm /etc/cron.d/rootfirstboot" > /etc/cron.d/rootfirstboot
chmod 700 /etc/cron.d/rootfirstboot

rm firstBoot

reboot
