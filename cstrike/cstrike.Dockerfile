FROM docker.io/lagertonne/cstrike:latest

COPY config_files/* /root/cs16/cstrike/
COPY html/maps/*.bsp /root/cs16/cstrike/maps/
COPY html/maps/*.txt /root/cs16/cstrike/maps/
COPY html/maps/*.res /root/cs16/cstrike/maps/
COPY html/*.wad /root/cs16/cstrike/
COPY html/gfx/env/*.tga /root/cs16/cstrike/gfx/env/
COPY html/models /root/cs16/cstrike/models/
COPY html/overviews /root/cs16/cstrike/overviews/
COPY html/sound /root/cs16/cstrike/sound/
COPY html/sprites /root/cs16/cstrike/sprites/
COPY addons /root/cs16/cstrike/addons/

ENTRYPOINT ["./hlds_run", "-game cstrike", "-insecure", "-port 27016", "-maxplayers 32"]
