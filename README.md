# WAN Party Dedicated Servers

### Requirements
* a Linux server running Docker
* upwards of 67 gigs of drive space (including the OS, not including spare buffer space).


### Getting Started
This first section of steps only needs to be done once
1. Create a server on [Vultr](https://my.vultr.com) (other services are fine too, but these instructions are probably easiest)
	* Choose cloud compute with amd or intel high performance
	* choose a location physically close
	* Choose a recent/LTS linux host (I went with Ubuntu 22.04 lTS)
	* choose a server size with at least 80GB of space (might need more, but I think 80 is good - I did 180 GB NVMe)
		* might seem expensive, but it's only a couple bucks for a day or two
	* disable backups
	* select `WAN Party Dedicated Server Deployment` from the boot scripts
	* deploy it
1. ssh in as root (putty on windows)
1. `cd` into the repo (probably `wan-party-dedicated-servers` unless you renamed it to something else)
1. run `dc --profile allServers up -d`
	* this can take a while. I've clocked it in at an hour once, but hopefully that was an extreme case. I would anticipate at LEAST 10-20 minutes though.

The servers have all been set up to use the `web` server in the `docker-compose.yml` file to distribute assets.

### Modifying Maps and Mods
1. ssh in as root (putty on windows)
1. `cd` into the repo (probably `wan-party-dedicated-servers` unless you renamed it to something else)
1. the following info is just reference and in no particular order:
	* `git pull`
		* update changes from gitlab
	* `dc --profile allServers down -t 1`
		* stop all the servers
	* `dc --profile allServers build`
		* build all servers
	* `dc --profile allServers up -d`
		* start all servers (and build, if necessary)
	* `dc up cssource cstrike -d`
		* start only the `cssource` and `cstrike` services (`web` will automatically boot as well since it's required)
	* you can chain these together in a single command by separating with a `;`
		* e.g. `dc --profile allServers down -t 1; dc build cssource; dc up cssource -d`


### Mods and Maps
#### Counter Strike 1.6
* Service name: `cstrike`
* Port: `27016`
* Mods
	* metamod
	* amxmodx
* Maps
	* etc

#### Counter Strike: Source
* Service name: `cssource`
* Port: `27015`
* Mods
	* sourcemod
* Maps
	* ...
* Rcon Commands
	* `sm_votemap <mapname> [mapname2] ... [mapname5] `
	* `sm_vote <question> [Answer1] [Answer2] ... [Answer5]`
	* `!models` in chat to change skin
	* `sm plugins load|unload <plugin name>` to add|remove plugin
		* Useful plugins to alter: `sm_ggdm_spawns`
* Plug-Ins
	* Surf Tools
		* https://forums.alliedmods.net/showthread.php?p=522844
	* Hide and Seek
		* https://forums.alliedmods.net/showthread.php?p=2647181

#### Counter Strike: GO
* Service name: `csgo`
* Port: `27017`
* Mods
	* ...
* Maps
	* ...

#### Team Fortress 2
* Service name: `tf2`
* Port: `27014`
* Mods
	* ...
* Maps
	* ...

#### Urban Terror 4
* Service name: `uterror`
* Port: `27960`
* Mods
	* it IS one
* Maps
	* ...

#### Quake 3
* Service name: `quake3`
* Port: see mods
* Mods
	* each mod is running a separate instance of q3 server on a unique port
		* matrix `27964`
		* urban terror 2.4 `27962`
		* urban terror 3.2 `27963`
		* quake 3 arena 	`27961`
* Maps
	* ...

### Note for Quake3 and Derivatives
It's somewhat unclear what file these go in. Normally they'd go in a cfg file in the quake/mod directory, but I think the modern build of quake's engine looks to the OS's user directory (AppData on windows and Library/Application Support for macOS, I think). Either way, here are the lines that you can use to modernize your resolution, once you figure out the file:

```cfg
seta r_mode -1
seta r_customwidth 1440
seta r_customheight 900
seta cg_fov 90
```

##### Boot Script
This is the boot script to place in the vultr scripts:

```sh
#!/usr/bin/env bash

export DUCKDNSHOST=wanparty
export DUCKDNSTOKEN=[insert duck dns token no brackets]
export STEAMID=[key here no brackets]
export PREBUILD_SERVICES="[semi-colon separated services, keep quotes, delete square brackets]"
#export PREBUILD_SERVICES="cssource;cstrike;uterror"

curl -o init.sh https://gitlab.com/mredig/wan-party-dedicated-servers/-/raw/main/init.sh
chmod +x init.sh
./init.sh
```