FROM docker.io/ubuntu:16.04

# Update the repository and install SteamCMD
ARG DEBIAN_FRONTEND=noninteractive

RUN dpkg --add-architecture i386
RUN apt-get update
RUN apt-get install -y \
curl \
tar \
libxml2-utils \
gzip 

COPY UrbanTerror43_ded.tgz /
RUN tar -zxvf UrbanTerror43_ded.tgz
WORKDIR UrbanTerror43
RUN ./UrTUpdater_Ded.sh -q


COPY config_files/* /UrbanTerror43/q3ut4/
COPY html/* /UrbanTerror43/q3ut4/

ENTRYPOINT ["./Quake3-UrT-Ded.x86_64", "+set fs_game q3ut4", "+set fs_basepath /UrbanTerror43/", "+set fs_homepath /UrbanTerror43/", "+set dedicated 2", "+set net_port 27960", "+set com_hunkmegs 256", "+exec server.cfg"]
